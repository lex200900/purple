import React from 'react';
import "./main.scss";
import logo from './img/expologo.png'
import logOut from './img/log-out.png'

function Header(props) {

    const navList = [
        {
            title: 'Курсы'
        },
        {
            title: 'Акселератор'
        },
        {
            title: 'Нетворкинг'
        },
        {
            title: 'Контакты'
        },
    ];

    return (
        <nav className="navbar navbar-expand-lg navbar-dark">
                <a className="navbar-brand" href="#">
                    <img src={logo} alt=""/>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        {navList.map(item => {
                            return(
                                <li className="nav-item">
                                    <a className="nav-link" href="#">{item.title}</a>
                                </li>
                            )
                        })}
                        <li className="nav-item">
                            <a className="nav-link nav-auth" href="#">
                                <img src={logOut} alt=""/>Войти</a>
                        </li>
                    </ul>
                </div>
            </nav>
    );
}

export default Header;