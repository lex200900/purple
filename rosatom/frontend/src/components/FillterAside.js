import React from 'react';

function FillterAside(props) {
    const coursList = [
        {
            cours: 'Дизайн'
        },
        {
            cours: 'Программирование'
        },
        {
            cours: 'Медицина'
        },
        {
            cours: 'Экономика'
        },
        {
            cours: 'Менеджмент'
        },
        {
            cours: 'Образование'
        },
        {
            cours: 'Продажи'
        },
        {
            cours: 'Строительство'
        },
        {
            cours: 'Финансы'
        },
        {
            cours: 'Другое'
        },
    ];
    return (
        <div className="col-lg-2 fillter-aside">
            <div className="fillter-aside__direction">
                <p className="fillter-aside__direction-title">Направление</p>
                <div className="fillter-aside__direction-items">
                    {coursList.map(item => {
                        return(
                            <div className="item"><input type="checkbox" id="design"/><label>{item.cours}</label></div>
                        )
                    })}
                </div>
            </div>
            <div className="fillter-aside__duration">
                <p className="fillter-aside__direction-title">Длительность</p>
                <label htmlFor="range">От 1 до 6 месяцев</label>
                <input type="range" id="range"/>
            </div>

            <div className="fillter-aside__level">
                <p className="fillter-aside__direction-title">Вид стажировки</p>
                <div className="item"><input type="radio" id="junior-level"/><label htmlFor="junior-level">Оплачиваемая</label></div>
                <div className="item"><input type="radio" id="senior-level"/><label htmlFor="senior-level">Неоплачиваемая</label></div>
            </div>

            <div className="fillter-aside__level">
                <p className="fillter-aside__direction-title">Опыт работы</p>
                <div className="item"><input type="radio" id="junior-level"/><label htmlFor="junior-level">Нет опыта</label></div>
                <div className="item"><input type="radio" id="senior-level"/><label htmlFor="senior-level">От 1 до 3 лет</label></div>
            </div>
        </div>
    );
}

export default FillterAside;