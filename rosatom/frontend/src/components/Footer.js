import React from 'react';
import youtube from "./img/you.png"
import inl from "./img/in.png"
import inst from "./img/inst.png"
import logo from './img/expologo.png'

function Footer(props) {
    return (

        <div className="row footer">
            <div className="footer-top">
                <img src={logo} alt="" className="footer-top__img"/>
                    <div className="footer-top__social">
                        <img src={inst} alt=""/>
                            <img src={inl} alt=""/>
                                <img src={youtube} alt=""/>
                    </div>
            </div>

            <div className="footer-bottom">
                <nav className="navbar navbar-expand-lg navbar-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Курсы</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Акселератор</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Нетворкинг</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <a href="#" className="footer-bottom__call">+ 7 (928) 123-45-67</a>
            </div>
        </div>
    );
}

export default Footer;