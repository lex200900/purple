import React from 'react';
import clock from './img/clock (1).png';
import portf from './img/briefcase-alt.png';
import compas from './img/compass.png';

function FillterContent(props) {

    const filterItem = [
        {
            title: 'Администратор',
            text: 'Организация работы офиса. Первичная документация. Решение административных вопросов. Заключение и сопровождение договоров. Контроль показателей работы перс...',
            term: '2 месяца',
            direction: 'Менеджмент',
            experience: 'Без опыта'
        },
        {
            title: 'UI/UX дизайнер',
            text: 'Отрисовка дизайн-макетов страниц для основного сайта. Отрисовка элементов интерфейса (UI). Подготовка визуального контента для социальных сетей',
            term: '2 месяца',
            direction: 'Менеджмент',
            experience: 'Без опыта'
        },
        {
            title: 'Продавец-консультант',
            text: 'Консультация покупателей по ассортименту, помощь в выборе товаров. Работа в торговом зале: выкладка товара, размещение актуальных ценников, поддержание порядка',
            term: '2 месяца',
            direction: 'Менеджмент',
            experience: 'От 1 до 3 лет'
        },
        {
            title: 'Проектный менеджер',
            text: 'Работать над проектами и задачами компании. Работать с большим объемом информации. Вести отчетность о проделанной работе.\n' +
                'Готовы рассмотреть кандидатов...',
            term: '2 месяца',
            direction: 'Менеджмент',
            experience: 'Без опыта'
        },
    ]

    return (
        <div className="col-lg-9 fillter-content">

                {filterItem.map(item => {
                    return(
                        <div className="fillter-courses modelirovanie">
                        <div className="courses fillter-courses__info">
                            <a href="cours.html"><p className="fillter-courses__info-title">{item.title}</p></a>
                            <p className="fillter-courses__info-subtitle">{item.text}</p>
                            <div className="fillter-courses__info-training" data-month="3">
                                <a href="#">
                                    <img src={clock} alt=""/>
                                    {item.term}
                                </a>
                                <a href="#">
                                    <img src={compas} alt=""/>
                                    {item.direction}
                                </a>
                                <a href="#">
                                    <img src={portf} alt=""/>
                                    {item.experience}
                                </a>
                            </div>
                        </div>
                        </div>
                    )
                })}

        </div>
    );
}

export default FillterContent;