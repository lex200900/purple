import React from 'react';
import FillterAside from "./FillterAside";
import FillterContent from "./FillterContent";

function Filter(props) {
    return (
        <div>
            <h2 className='fillter-title'>Стажировки по всем направлениям</h2>
            <div className="fillter">
                <FillterAside/>
                <FillterContent/>
            </div>
        </div>

    );
}

export default Filter;