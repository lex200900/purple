import React from 'react';
import "./main.scss";
import logoBg from './img/bg.png'

function MainBg(props) {
    return (
        <main className="main container">
            <div className="main-content">
                <h1 className="main-content__title">
                    Стажировки и практика для
                     <span> студентов</span> вузов и ссузов
                </h1>
                <p className="main-content__suptitle">
                    Международная IT-компания, предлагающая услуги в
                    сфере разработки программного обеспечения, обучения
                    IT-специалистов и продвижения начинающих проектов.
                </p>
            </div>
            <img src={logoBg} alt="" className="bg-banner"/>
        </main>
    );
}

export default MainBg;