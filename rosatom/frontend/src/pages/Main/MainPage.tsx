import React, { useState, useRef } from 'react';
import classNames from "classnames/bind";
import Grid from '@material-ui/core/Grid';
import styles from './MainPage.module.scss';
import { Link } from 'react-router-dom';
import Header from "../../components/Header";
import MainBg from "../../components/MainBg";
import Filter from "../../components/Filter";
import Footer from "../../components/Footer";



const cx = classNames.bind(styles);

const MainPage = () => {
    return (
        <Grid className={cx('container')} container>
            <Header/>
            <MainBg/>
            <Filter/>
            <Footer/>
          {/*<Link to="/home">Hello</Link>*/}
          {/*<Link to="/second-page">Second Page</Link>*/}
          {/* MainPage*/}
        </Grid>
    );
};

export {MainPage};
